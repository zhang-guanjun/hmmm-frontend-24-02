import * as Vue from 'vue'
module.exports = (file) =>
  Vue.defineAsyncComponent(() => import('@/module-' + file + '.vue'))
