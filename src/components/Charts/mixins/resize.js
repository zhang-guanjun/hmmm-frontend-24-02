import { debounce } from '@/utils'

export default {
  mounted () {
    this.__resizeHanlder = debounce(() => {
      if (this.chart) {
        this.chart.resize()
      }
    }, 100)
    window.addEventListener('resize', this.__resizeHanlder)
  },
  beforeUnmount () {
    window.removeEventListener('resize', this.__resizeHanlder)
  }
}
