// import moment from 'moment'
export function question (val) {
  // 1 2 3 单选 多选 简答
  if (val === '1') val = '单选'
  if (val === '2') val = '多选'
  if (val === '3') val = '简答'
  return val
}

export function difficultyFn (val) {
  // 1 2 3 简单 一般 困难
  if (val === '1') val = '简单'
  if (val === '2') val = '一般'
  if (val === '3') val = '困难'
  return val
}

export function chkStateFn (val) {
  // 0 1 2 待审核 通过 拒绝
  if (val === 0) val = '待审核'
  if (val === 1) val = '已审核'
  if (val === 2) val = '已拒绝'
  return val
}

export function publishStateFn (row) {
  // 1 0 上架 下架
  if (row.publishState === 1 && row.chkState === 1) return '已发布'
  if (row.publishState === 0) return '已下架'
  return '待发布'
}

// export function formatDate (val, format = 'YYYY-MM-DD') {
//   if (val) {
//     return moment(val).format(format)
//   } else {
//     val
//   }
// }
