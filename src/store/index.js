import * as Vuex from 'vuex'
import getters from './getters'

const store = Vuex.createStore({
  getters
})

export default store
