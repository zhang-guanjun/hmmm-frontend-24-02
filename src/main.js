import * as Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Cookies from 'js-cookie'
import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import ElementPlus, { avatarProps } from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'// 字图
import '@/styles/index.scss' // global css
// font-awesome
import 'font-awesome/css/font-awesome.css'
import './mock' // simulation data
import i18n from './lang/index.js'

import * as filters from './filters' // global filters
import './icons' // icon
import SvgIcon from '@/components/SvgIcon'// svg component
/* eslint-disable */

const app = Vue.createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
// 全局注册quill富文本
import { quillEditor, Quill } from 'vue3-quill'
app.use(quillEditor)
// icon图标
// 此处已删除
// import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
//   app.component(key, component)
// }

/*
 * 注册 - 业务模块
 */
import dashboard from '@/module-dashboard/' // 面板
import base from '@/module-manage/' // 用户管理
import hmmm from '@/module-hmmm/' // 黑马面面

app.use(dashboard, store)
app.use(base, store)
app.use(hmmm, store)
// 过滤器
app.config.globalProperties.$filters = {}
Object.keys(filters).forEach((key) => {
  app.config.globalProperties.$filters[key] = filters[key]
})
app.config.globalProperties.routerAppend = (path, pathToAppend) => {
  return path + (path.endsWith('/') ? '' : '/') + pathToAppend
}
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import en from 'element-plus/es/locale/lang/en'
app.component('SvgIcon', SvgIcon)
app.use(ElementPlus, {
  locale: Cookies.get('language') ==='en'? en : zhCn,
})
app.use(store)
app.use(i18n)
app.use(router)
app.mount('#app')
